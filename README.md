Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com <br />
Copyright © 2015 Théo Torcq, ceto.rcq@gmail.com <br />
https://gitlab.com/rodolphe-c/C3PO-physic-tests

/!\ PAUSED PROJECT

# C3PO

This project brings together various aspects about the laws of physics. <br />
You can download exemples here :

```
https://gitlab.com/rodolphe-c/C3PO-physic-tests
```

### System Requirement

Required:
- C++11 compiler
- hopp
